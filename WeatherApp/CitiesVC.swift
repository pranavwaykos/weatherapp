//
//  CitiesVC.swift
//  WeatherApp
//
//  Created by Mark Maged on 11/25/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

class CitiesVC: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, CityDetailsVCDelegate
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - iVars
    
    var cities = [City]()
    var filteredCities = [City]()
    var filterMode = false
    var predicate: NSPredicate!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Show navBar
        self.navigationController?.setNavigationBarHidden(false,
                                                          animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Configure searchBar
        self.searchBar.setTextColor(color: UIColor.white)
        self.searchBar.setPlaceholderTextColor(color: UIColor.white)
        self.searchBar.setSearchImageColor(color: UIColor.white)
        self.searchBar.setTextFieldClearButtonColor(color: UIColor.white)
    }
    
    // MARK: - UITableViewDataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.filterMode
        {
            return self.filteredCities.count
        }
        else
        {
            return self.cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell",
                                                 for: indexPath) as! CityCell
        var city: City!
        
        if self.filterMode
        {
            city = self.filteredCities[indexPath.row]
        }
        else
        {
            city = self.cities[indexPath.row]
        }
        
        cell.configureCell(city: city)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 138.5
    }
    
    // MARK: - UITableViewDelegate Methods
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
    }
    
    // MARK: - UISearchBarDelegate Methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        NSObject.cancelPreviousPerformRequests(withTarget: self,
                                               selector: #selector(self.filterData(withText:)),
                                               object: searchBar.text)
        
        self.perform(#selector(self.filterData(withText:)),
                     with: searchBar.text,
                     afterDelay: 0.5)
    }
    
    // To dismiss keyboard on scrolling
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "CityDetailsVC",
            let cityDetailsVC = segue.destination as? CityDetailsVC
        {
            cityDetailsVC.delegate = self
            
            let indexPath = self.tableView.indexPathForSelectedRow
            
            if self.filterMode
            {
                cityDetailsVC.city = self.filteredCities[(indexPath?.row)!]
            }
            else
            {
                cityDetailsVC.city = self.cities[(indexPath?.row)!]
            }
        }
    }
    
    // MARK: - CityDetailsVCDelegate Methods
    
    func dismissCityDetailsVC()
    {
        self.dismiss(animated: true,
                     completion: nil)
    }
    
    // MARK: - Helper Methods
    
    /// Filter Data from local array of cities
    ///
    /// - Parameter text: Text that user typed in
    @objc func filterData(withText text: String)
    {
        if text != ""
        {
            self.predicate = NSPredicate(format: "SELF contains[c] %@", text)
            
            self.filteredCities = self.cities.filter { self.predicate.evaluate(with: $0.name) }
            
            self.filterMode = true
            self.tableView.reloadData()
            
            // To prevent the app from crashing if no data match the search - scrolling to first row which does not exists -
            if self.filteredCities.count > 0
            {
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0),
                                           at: .top,
                                           animated: true)
            }
        }
        else
        {
            self.filterMode = false
            self.tableView.reloadData()
        }
    }
}

// MARK: - UISearchBar Extension

extension UISearchBar
{
    private func getViewElement<T>(type: T.Type) -> T?
    {
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        
        return element
    }
    
    func getSearchBarTextField() -> UITextField?
    {
        return getViewElement(type: UITextField.self)
    }
    
    func setTextColor(color: UIColor)
    {
        if let textField = getSearchBarTextField()
        {
            textField.textColor = color
        }
    }
    
    func setTextFieldColor(color: UIColor)
    {
        if let textField = getViewElement(type: UITextField.self)
        {
            switch searchBarStyle
            {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
                
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
    
    func setPlaceholderTextColor(color: UIColor)
    {
        if let textField = getSearchBarTextField()
        {
            textField.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: color])
        }
    }
    
    func setTextFieldClearButtonColor(color: UIColor)
    {
        if let textField = getSearchBarTextField()
        {
            let button = textField.value(forKey: "clearButton") as! UIButton
            
            if let image = button.imageView?.image
            {
                button.setImage(image.transform(withNewColor: color), for: .normal)
            }
        }
    }
    
    func setSearchImageColor(color: UIColor)
    {
        if let imageView = getSearchBarTextField()?.leftView as? UIImageView
        {
            imageView.image = imageView.image?.transform(withNewColor: color)
        }
    }
}

// MARK: - UIImage Extension

extension UIImage
{
    func transform(withNewColor color: UIColor) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
