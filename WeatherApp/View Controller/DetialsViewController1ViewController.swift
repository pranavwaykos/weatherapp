//
//  DetialsViewController1ViewController.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 13/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetialsViewController1ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var cityDetailsModel:City!
    var weather:[DeatailCity] = []
    @IBOutlet weak var tableView: UITableView!
  
    override func viewDidLoad() {
        let nib = UINib(nibName: "WeatherTableViewCell",bundle:nil)
        tableView.register(nib, forCellReuseIdentifier: "WeatherTableViewCell")
        super.viewDidLoad()
        apirequest(city: self.cityDetailsModel.name!)
    }
            func apirequest(city:String) {
                let  Url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=\(city)&appid=c9b1366a58703be766cd459c28205d36")
                var citymintemp:String! = "",citymaxtemp:String! = "" ,id:String! = "",dt_txt:String!="",pressure:String!,descre:String!="", five_minTemp:String! = ""
                    Alamofire.request(Url!).validate().responseJSON { (response) in
                            if ((response.result.value) != nil) {

                                let jsondata = JSON(response.result.value!)
                                    print(jsondata)
                                if let dict =  response.result.value! as? NSDictionary {
                                    var arr = dict.value(forKey:"list") as! NSArray
                                    for each in arr {
                                        if let obj = each as? NSDictionary {
                                            if let mainObj = (obj).value(forKey: "main") {
                                                if let name = ((mainObj) as AnyObject).value(forKey: "temp_min")  {
                                                    citymintemp = String (describing :name)

                                    if let mainObj = (obj).value(forKey: "main") {
                                        if let name = ((mainObj) as AnyObject).value(forKey: "temp_max")  {
                                            citymaxtemp = String (describing :name)

                                            if let weatherArr = (obj).value(forKey: "weather") as? NSArray {
                                                for each in weatherArr {
                                                    if let weatherObj = each as? NSDictionary {
                                                        if let des = weatherObj.value(forKey: "description") {
                                                            print(des)
                                                        }
                                                    }
                                                    if let name = ((obj) as AnyObject).value(forKey: "weather")  {
                                                        dt_txt = String (describing :name)
                                                        if let name = ((obj) as AnyObject).value(forKey: "dt_txt")  {
                                                            dt_txt = String (describing :name)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if let weatherArr = (obj).value(forKey: "weather") as? NSArray {
                                            for each in weatherArr {
                                                if let weatherObj = each as? NSDictionary {
                                                    if let name = weatherObj.value(forKey: "description") {
                                                         descre = String (describing :name)

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      let newobj = DeatailCity(mintemp: citymintemp, maxtemp: citymaxtemp, pressure: dt_txt, descre: descre)
                    self.weather.append(newobj)
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
     // MARK: - TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as! WeatherTableViewCell
      //  let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.cityDetailsObject = weather[indexPath.row]
        cell.setLabelValues()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
