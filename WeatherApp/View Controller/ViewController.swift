//
//  ViewController.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 12/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var newgif: UIImageView!
    @IBOutlet weak var dateandtimeLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var cityname: UILabel!
    @IBOutlet weak var templabel: UILabel!
    @IBOutlet weak var humidtylabel: UILabel!
    var newcity:String = ""
    var cityList:[GraphTemprature] = []
    var cityModel:City!
    var city =  [""]
    override func viewDidLoad() {
        
        //newgif.loadGif(name:"background")
                        super.viewDidLoad()
                        searchBar.delegate = self
                        searchBar.returnKeyType = UIReturnKeyType.done
                        getCurrentDateTime()
                    let date = Date()
                        let formatter = DateFormatter()
                        formatter.dateStyle = .long
                        formatter.timeStyle = .medium
                        formatter.dateFormat = "dd/MM/yyyy     HH:mm"
                    dateandtimeLabel.text = "\(formatter.string(from: date))"
        }
    //MARK:- User API call
            func httpUsingAlamoFire(city:String) {
                let  Url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=c9b1366a58703be766cd459c28205d36")
                    print(city)
                Alamofire.request(Url!).validate().responseJSON { (response) in
                    if ((response.result.value) != nil) {
                        let jsondata = JSON(response.result.value!)
                            print(jsondata)
                if let obj =  response.result.value! as? NSDictionary {
                    var cityName:String! = "" , cityTemp:String! = "" , humidity:String! = "",id:String! = ""
                        if let flag = (obj).value(forKey: "name")  {
                        cityName = String (describing :flag)
                        self.cityname.text = cityName
                    }
                    if let flag = (obj).value(forKey: "id")  {
                        id = String (describing :flag)
                    }
                    if let mainObj = (obj).value(forKey: "main") {
                        if let name = ((mainObj) as AnyObject).value(forKey: "temp")  {
                            cityTemp = String (describing :name)
                            self.templabel.text = cityTemp
                        }
                        if let name = ((mainObj) as AnyObject).value(forKey: "humidity")  {
                            humidity = String (describing :name)
                            self.humidtylabel.text = humidity
                        }
                    }
                    self.cityModel = City(id: id, name: cityName, temp: cityTemp, hum: humidity)
                    print("\(cityName)  \(cityTemp)  \(humidity)")
                
                }
            }
        }
    }
    func getCityWeather(cityId:String) {
        let  Url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=c9b1366a58703be766cd459c28205d36\(cityId)")
        Alamofire.request(Url!).validate().responseJSON { (response) in
            if ((response.result.value) != nil) {
                let jsondata = JSON(response.result.value!)
                print(jsondata)

            }
        }
    }

    //MARK:- User Actions
    @IBAction func seachButton(_ sender: Any) {
  
        let city = searchBar.text
        self.newcity = searchBar.text!
        httpUsingAlamoFire(city:city!)
        print(city!)
    }
    @IBAction func detialViewAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DetialsViewController1ViewController") as! DetialsViewController1ViewController
        controller.cityDetailsModel = self.cityModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
  

    func getCurrentDateTime (){
    }

    @IBAction func graphVIew(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "GraphViewController") as! GraphViewController
        controller.newcity = newcity
        self.navigationController?.pushViewController(controller, animated: true)
    }
}






