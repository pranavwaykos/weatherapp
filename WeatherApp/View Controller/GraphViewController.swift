//
//  GraphViewController.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 15/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//
import UIKit
import SwiftyJSON
import Alamofire
import SwiftCharts

class GraphViewController: UIViewController {
    
    
    // MARK: - Outlets and Variables Declaration
    var newcity:String = ""
    var cityList:[GraphTemprature] = []
    var lintChart:LineChart!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = ("\(newcity) Weather Graph")
        getnextFiveDayWeatherApi(city:newcity)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Chart Initialization
    func chartInitialization() {
        let chartConfiguration = ChartConfigXY(
            xAxisConfig: ChartAxisConfig(from: cityList[0].g_day, to: cityList[cityList.count-1].g_day, by: 1) ,
            yAxisConfig: ChartAxisConfig(from: 0 , to: 100 , by:8)
        )
        let chartFrame = CGRect(x: 0 , y: 190 , width: self.view.frame.width-10 , height: 450)
        let chart = LineChart (
            frame: chartFrame,
            chartConfig: chartConfiguration,
            xTitle: "Day",
            yTitle: "Units",
            lines: [
                (chartPoints: [(cityList[0].g_day,cityList[0].g_minTemp),(cityList[9].g_day,cityList[9].g_minTemp),(cityList[15].g_day,cityList[15].g_minTemp),(cityList[15].g_day,cityList[23].g_minTemp),(cityList[30].g_day,cityList[30].g_minTemp),(cityList[35].g_day,cityList[35].g_minTemp)],color: UIColor.green),
                (chartPoints:[(cityList[0].g_day,20),(cityList[9].g_day,30),(cityList[15].g_day,60),(cityList[15].g_day,40),(cityList[30].g_day,20),(cityList[35].g_day,55)],color: UIColor.red),
               (chartPoints: [(cityList[0].g_day,cityList[0].g_humidity),(cityList[9].g_day,cityList[9].g_humidity),(cityList[15].g_day,cityList[15].g_humidity),(cityList[15].g_day,cityList[23].g_humidity),(cityList[30].g_day,cityList[30].g_humidity),(cityList[35].g_day,cityList[35].g_humidity)],color: UIColor.yellow)
            ]
        )
        self.view.addSubview(chart.view)
        self.lintChart = chart
        
    }

    // MARK: - Api Calls
    
    func getnextFiveDayWeatherApi(city:String) {
        var five_minTemp:Double = 0 , five_maxTemp:Double = 0 , five_humidity:Double = 0 , five_day:Double = 0
        let doubleValue:Double = 273.15
        let  Url = URL(string:"http://api.openweathermap.org/data/2.5/forecast?q=\(city)&appid=c9b1366a58703be766cd459c28205d36")
            Alamofire.request(Url!).validate().responseJSON { (response) in
                if ((response.result.value) != nil) {
                    let jsondata = JSON(response.result.value!)
                    print(jsondata)
            if let object = response.result.value! as? NSDictionary {
                let weatherArray = object.value(forKey:"list") as! NSArray
                    for each in weatherArray {
                        if let obj = each as? NSDictionary {
                if let mainObj = (obj).value(forKey: "main") {
                    if let minTemp = ((mainObj) as AnyObject).value(forKey: "temp_min")  {
                        let city_minTemp = String (describing :minTemp)
                            let celsiusTemp = Double(city_minTemp)! - doubleValue
                            let min_temprature = String(format: "%.0f", celsiusTemp)
                                five_minTemp = Double(min_temprature)!
                                }
                if let maxTemp = ((mainObj) as AnyObject).value(forKey: "temp_max")  {
                    let city_maxTemp = String (describing :maxTemp)
                        let celsiusTemp = Double(city_maxTemp)! - doubleValue
                            let max_temprature = String(format: "%.0f", celsiusTemp)
                                five_maxTemp = Double(max_temprature)!
                                }
                if let humidity = ((mainObj) as AnyObject).value(forKey: "humidity")  {
                    let city_hum = String (describing :humidity)
                    five_humidity = Double(city_hum)!
                    }
                }
                if let date_time = (obj).value(forKey: "dt_txt") {
                    let dateTime = String (describing :date_time)
                        print(dateTime)
                            let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-DD HH:mm:ss"
                    let date = dateFormatter.date(from: dateTime)
                        dateFormatter.dateFormat = "yyyy-MM-DD"
                            let calendar = Calendar.current
                            let components = calendar.dateComponents([.year, .month, .day], from: date!)
                                let day = components.day
                            five_day = Double(day!)
                           }
                        }
                let tempratureObject = GraphTemprature(g_minTemp:five_minTemp, g_maxTemp: five_maxTemp, g_humidity: five_humidity,g_day: five_day)
                        self.cityList.append(tempratureObject)
                    }
            DispatchQueue.main.async {
                        }
               self.chartInitialization()
                }
            }
        }
    }
}

