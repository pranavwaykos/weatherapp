//
//  GraphTemprature.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 15/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//

import Foundation
class GraphTemprature : NSObject {
    var g_minTemp:Double,g_maxTemp:Double,g_humidity:Double,g_day:Double
    override init() {
        self.g_day = 0
        self.g_humidity = 0
        self.g_maxTemp = 0
        self.g_minTemp = 0
    }
    
    init(g_minTemp:Double,g_maxTemp:Double,g_humidity:Double,g_day:Double) {
        self.g_minTemp = g_minTemp
        self.g_maxTemp = g_maxTemp
        self.g_day = g_day
        self.g_humidity = g_humidity
    }
}

