//
//  City.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 12/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//

import Foundation
class City {
    var id: String?  // ID is relate to openWeatherMap Database
    var name: String?
    var temp:String?
    var hum:String?
    
    init() {}
    
    init(id: String?, name: String?,temp:String?, hum:String?) {
        self.id = id
        self.name = name
        self.temp = temp
        self.hum = hum
    }
}
