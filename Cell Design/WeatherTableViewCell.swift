//
//  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Tejas Inamdar on 12/02/18.
//  Copyright © 2018 Tejas Inamdar. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    var cityDetailsObject:DeatailCity!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var tempValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setLabelValues()
    {
        cityName.text = self.cityDetailsObject.descre
        temp.text = self.cityDetailsObject.mintemp
        humidity.text = self.cityDetailsObject.maxtemp
        tempValue.text = self.cityDetailsObject.pressure
        
    }
}
